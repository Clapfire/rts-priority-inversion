# Real Time Systems Priority Inversion

This repo contains the code to demonstrate the priority inversion problem in real time scheduling.

There are 3 files.

`priorityInversion.cpp` demonstrates the problem.

`priorityInversionCeiling.cpp` demonstrates how a priority ceiling can fix it.

`priorityInversionInheritance.cpp` demonstrates how priority inheritance can fix it.

You can use `make` to compile these files. They are designed to be executed on a Raspberry Pi, on a single core. For example, if you want to compile and run the original problem, do the following:

`make priorityInversion`\
`sudo taskset -c 0 ./priorityInversion`
