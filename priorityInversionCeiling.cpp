#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#define BURN 89000000

pthread_mutex_t resource;

static void CPUburn(unsigned long n)
{
    for (unsigned long i = 0; i <= n; i++)
    {
        // do nothing
    }
}

void work(int time_units, int task_no, char output)
{
    static unsigned int globalTime = 0;
    for (int j = 0; j < time_units; j++)
    {
        printf("%d:", globalTime);
        globalTime++;
        for (int j = 0; j < task_no; j++)
            printf("\t");
        printf("%d%c\n", task_no, output);
        fflush(stdout);
        CPUburn(BURN);
    }
}

static void *thread_0(void *vptr)
{
    int thread_no = (int)vptr;

    while (1)
    {
        work(1, thread_no, ' ');
        sleep(1);
    }

    return NULL;
}

static void *thread_1(void *vptr)
{
    int thread_no = (int)vptr;

    while (1)
    {
        sleep(1);
        pthread_mutex_lock(&resource);
        work(1, thread_no, 'C');
        work(3, thread_no, 'R');
        pthread_mutex_unlock(&resource);
        work(2, thread_no, ' ');
    }

    return NULL;
}

static void *thread_2(void *vptr)
{
    int thread_no = (int)vptr;

    while (1)
    {
        sleep(2);
        work(8, thread_no, ' ');
    }

    return NULL;
}

static void *thread_3(void *vptr)
{
    int thread_no = (int)vptr;

    while (1)
    {
        sleep(5);
        work(2, thread_no, ' ');
        pthread_mutex_lock(&resource);
        work(1, thread_no, 'C');
        work(3, thread_no, 'R');
        pthread_mutex_unlock(&resource);
        work(2, thread_no, ' ');
    }

    return NULL;
}

int main(void){
    // Mutex parameters
    pthread_mutexattr_t m_attr;
    pthread_mutexattr_init(&m_attr);
    pthread_mutexattr_setprotocol(&m_attr, PTHREAD_PRIO_PROTECT);
    pthread_mutexattr_setprioceiling(&m_attr, 23);
    
    // Initialize mutex
    pthread_mutex_init(&resource, &m_attr);
    // Thread parameters
    struct sched_param params_t0, params_t1, params_t2, params_t3;

    params_t0.sched_priority = 19;
    params_t1.sched_priority = 20;
    params_t2.sched_priority = 21;
    params_t3.sched_priority = 22;

    pthread_attr_t attr_0, attr_1, attr_2, attr_3;

    pthread_attr_init(&attr_0);
    pthread_attr_init(&attr_1);
    pthread_attr_init(&attr_2);
    pthread_attr_init(&attr_3);

    pthread_attr_setinheritsched(&attr_0, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setinheritsched(&attr_1, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setinheritsched(&attr_2, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setinheritsched(&attr_3, PTHREAD_EXPLICIT_SCHED);

    pthread_attr_setschedpolicy(&attr_0, SCHED_FIFO);
    pthread_attr_setschedpolicy(&attr_1, SCHED_FIFO);
    pthread_attr_setschedpolicy(&attr_2, SCHED_FIFO);
    pthread_attr_setschedpolicy(&attr_3, SCHED_FIFO);

    pthread_attr_setschedparam(&attr_0, &params_t0);
    pthread_attr_setschedparam(&attr_1, &params_t1);
    pthread_attr_setschedparam(&attr_2, &params_t2);
    pthread_attr_setschedparam(&attr_3, &params_t3);

    // Create threads

    pthread_t t_0;
    pthread_t t_1;
    pthread_t t_2;
    pthread_t t_3;

    pthread_create(&t_0, &attr_0, thread_0, (void *)0);
    pthread_create(&t_1, &attr_1, thread_1, (void *)1);
    pthread_create(&t_2, &attr_2, thread_2, (void *)2);
    pthread_create(&t_3, &attr_3, thread_3, (void *)3);
    
    pthread_join(t_0, NULL);
    pthread_join(t_1, NULL);
    pthread_join(t_2, NULL);
    pthread_join(t_3, NULL);

    return 0;
}