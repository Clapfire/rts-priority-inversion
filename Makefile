# Makefile for Writing Make Files Example

# *****************************************************
# Variables to control Makefile operation

CXX = g++
CXXFLAGS = -lpthread

# ****************************************************
# Targets needed to bring the executable up to date

all: priorityInversion priorityInversionInheritance priorityInversionCeiling

priorityInversion: priorityInversion.cpp
	$(CXX) $(CXXFLAGS) -o priorityInversion priorityInversion.cpp

priorityInversionInheritance: priorityInversionInheritance.cpp
	$(CXX) $(CXXFLAGS) -o priorityInversionInheritance priorityInversionInheritance.cpp

priorityInversionCeiling: priorityInversionCeiling.cpp
	$(CXX) $(CXXFLAGS) -o priorityInversionCeiling priorityInversionCeiling.cpp

clean:
	rm priorityInversion priorityInversionCeiling priorityInversionInheritance